import { createAppContainer } from 'react-navigation';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import Home from '../Escenas/Home';
import Alertas from '../Escenas/Alertas';

const MainNavigator = createMaterialBottomTabNavigator(
    {
        Home: { screen: Home },
        Alertas: { screen: Alertas },
    },
    {
        initialRouteName: 'Home',
        activeColor: '#f0edf6',
        inactiveColor: '#3e2465',
        barStyle: { backgroundColor: 'green' },
    }
);

const BottomNavigation = createAppContainer(MainNavigator);

export default BottomNavigation;