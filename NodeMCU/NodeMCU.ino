#include <ArduinoJson.h>
#include <ESP8266HTTPClient.h>
#include <ESP8266WiFi.h>

int PINECHO = 2;  //D4
int PINTRIG = 0;  //D3
int PINLEDR = 4;  //D2
int PINLEDV = 5;  //D1
int PININFRARROJO = 15;  //D8

double distancia = 0;
int pulso = 0;
bool banderaInfrarrojo = true;

void setup() {

  pinMode(PINECHO, INPUT);
  pinMode(PINTRIG, OUTPUT);
  pinMode(PINLEDR, OUTPUT);
  pinMode(PINLEDV, OUTPUT);
  pinMode(PININFRARROJO, INPUT);

  Serial.begin(115200);                 //Serial connection
  WiFi.begin("WiFi-UAO", "");   //WiFi connection
  pinMode(A0, INPUT);
  while (WiFi.status() != WL_CONNECTED) {  //Wait for the WiFI connection completion

    delay(500);
    Serial.println("Waiting for connection");

  }
}

void loop() {

  //Ultrasonico
  // Clears the trigPin
  digitalWrite(PINTRIG, LOW);
  delayMicroseconds(2);

  // Sets the trigPin on HIGH state for 10 micro seconds
  digitalWrite(PINTRIG, HIGH);
  delayMicroseconds(10);
  digitalWrite(PINTRIG, LOW);

  // Reads the echoPin, returns the sound wave travel time in microseconds
  pulso = pulseIn(PINECHO, HIGH);

  // Calculating the distance
  distancia = pulso * 0.034 / 2;
  // Prints the distance on the Serial Monitor
  //Serial.print("Distance: ");
  //Serial.println(distancia);

  if (distancia > 5)
  {
    digitalWrite(PINLEDV, HIGH);
    digitalWrite(PINLEDR, LOW);
  } else
  {
    digitalWrite(PINLEDV, LOW);
    digitalWrite(PINLEDR, HIGH);
  }
  //Ultrasonico

  // put your main code here, to run repeatedly:
  if (WiFi.status() == WL_CONNECTED) { //Check WiFi connection status

    putDistancia();

    //Infrarrojo
    if (digitalRead(PININFRARROJO) == 0 && banderaInfrarrojo)
    {
      postInfrarrojo();
      banderaInfrarrojo = false;
      Serial.println("objeto detectado");
    }
    else if (digitalRead(PININFRARROJO) != 0)
    {
      banderaInfrarrojo = true;
      Serial.println("----------");
    }
    //Infrarrojo

  } else {
    Serial.println("Error in WiFi connection");
  }

}

void postInfrarrojo()
{
  HTTPClient http;    //Declare object of class HTTPClient
  WiFiClient client;
  http.begin(client, "http://11.11.8.170:8080/infrarrojo");      //Specify request destination
  http.addHeader("Content-Type", "application/json");  //Specify content-type header

  StaticJsonBuffer<300> JSONbuffer;
  JsonObject& doc = JSONbuffer.createObject();

  doc["idDispositivo"] = 1;

  char JSONmessageBuffer[300];
  doc.prettyPrintTo(JSONmessageBuffer, sizeof(JSONmessageBuffer));

  int httpCode = http.POST(JSONmessageBuffer);   //Send the request
  String payload = http.getString();                  //Get the response payload

  //Serial.println(httpCode);   //Print HTTP return code
  //Serial.println(payload);    //Print request response payload

  http.end();  //Close connection
}

void putDistancia()
{
  HTTPClient http;    //Declare object of class HTTPClient
  WiFiClient client;
  http.begin(client, "http://11.11.8.170:8080/distancia");      //Specify request destination
  http.addHeader("Content-Type", "application/json");  //Specify content-type header

  StaticJsonBuffer<300> JSONbuffer;
  JsonObject& doc = JSONbuffer.createObject();

  doc["idDistancia"] = 31;
  doc["valor"] = distancia;

  char JSONmessageBuffer[300];
  doc.prettyPrintTo(JSONmessageBuffer, sizeof(JSONmessageBuffer));

  int httpCode = http.PUT(JSONmessageBuffer);   //Send the request
  String payload = http.getString();                  //Get the response payload

  //Serial.println(httpCode);   //Print HTTP return code
  //Serial.println(payload);    //Print request response payload

  http.end();  //Close connection
}
