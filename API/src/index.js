// Cargar modulos y crear nueva aplicacion
var express = require("express");
var app = express();
var cors = require("cors");
var bodyParser = require("body-parser");
var mysql = require("mysql");

app.use(bodyParser.json()); // soporte para bodies codificados en jsonsupport
app.use(bodyParser.urlencoded({ extended: true })); // soporte para bodies codificados
app.use(cors());

// mysql://be106c72f7c50c:af8417f1@us-cdbr-iron-east-05.cleardb.net/heroku_d89721eefac1c73?reconnect=true
var mysqlConnection;

function handleDisconnect() {
  mysqlConnection = mysql.createConnection({
    host: "us-cdbr-iron-east-05.cleardb.net",
    user: "be106c72f7c50c",
    password: "af8417f1",
    database: "heroku_d89721eefac1c73"
  });

  mysqlConnection.connect(function (err) {
    if (err) {
      console.log("error when connecting to db:", err);
    } else {
      console.log("Se conectó a la Base de Datos");
    }
  });

  mysqlConnection.on('error', function (err) {
    console.log('db error', err);
    if (err.code === 'PROTOCOL_CONNECTION_LOST') { // Connection to the MySQL server is usually
      handleDisconnect();                         // lost due to either server restart, or a
    } else {                                      // connnection idle timeout (the wait_timeout
      throw err;                                  // server variable configures this)
    }
  });
}

app.get("/dispositivos", function (req, res, next) {
  mysqlConnection.query("SELECT * FROM dispositivo", (err, rows, fields) => {
    if (err) {
      console.log("=>" + err);
    } else {
      res.json(rows);
      console.log('get/distancia');
    }
  });
});

//Ejemplo: GET http://localhost:8080
app.get("/distancia/:idDispositivo", function (req, res, next) {
  var idDispositivo = req.params.idDispositivo;
  mysqlConnection.query("SELECT * FROM distancia WHERE fk_distancia_dispositivo = " + idDispositivo, (err, rows, fields) => {
    if (err) {
      console.log("=>" + err);
    } else {
      res.json(rows);
      console.log('get/distancia');
    }
  });
});

app.get("/infrarrojo/:idDispositivo", function (req, res, next) {
  const idDispositivo = req.params.idDispositivo;
  mysqlConnection.query("SELECT * FROM infrarrojo WHERE fk_infrarrojo_dispositivo = " + idDispositivo + " ORDER BY -ABS(fecha) limit 10", (err, rows, fields) => {
    if (err) {
      console.log("=>" + err);
    } else {
      res.json(rows);
      console.log('get/infrarrojo');
    }
  });
});

app.post("/distancia", function (req, res) {
  const body = req.body;
  const idDispositivo = body.idDispositivo;
  const valor = body.valor;

  const query =
    "INSERT INTO distancia (valor, fk_distancia_dispositivo) VALUES (?, ?);";
  mysqlConnection.query(
    query,
    [valor, idDispositivo],
    (err, rows, fields) => {
      if (err) {
        console.log("=>" + err);
      } else {
        res.send(rows);
        console.log('post/distancia');
      }
    }
  );
});

app.post("/infrarrojo", function (req, res) {
  const body = req.body;
  const idDispositivo = body.idDispositivo;

  var fecha = new Date();

  const query =
    "INSERT INTO infrarrojo (fecha, fk_infrarrojo_dispositivo) VALUES (?, ?);";
  mysqlConnection.query(
    query,
    [(fecha.getTime() / 1000), idDispositivo],
    (err, rows, fields) => {
      if (err) {
        console.log("=>" + err);
      } else {
        res.send(rows);
        console.log('post/infrarrojo');
      }
    }
  );
});

app.put("/distancia", function (req, res) {
  const body = req.body;
  const idDistancia = body.idDistancia;
  const valor = body.valor;
  const query =
    "UPDATE distancia SET valor=? WHERE id_distancia=?;";
  mysqlConnection.query(
    query,
    [valor, idDistancia],
    (err, rows, fields) => {
      if (err) {
        console.log("=>" + err);
      } else {
        res.send(rows);
        console.log('put/distancia');
      }
    }
  );
});

// //Ejemplo: DELETE http://localhost:8080/items
// app.delete("/items/:id", function (req, res) {
//   var itemId = req.params.id;
//   res.send("Delete " + itemId);
// });

app.listen(8080, function () {
  handleDisconnect();
  console.log("Server is running..");
});
